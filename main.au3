#include <String.au3>
#include <Array.au3>
#include <File.au3>
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include 'md5.au3'

TraySetIcon(@scriptdir & "\icon.ico")
Global $handle = FileOpen(@Scriptdir & "\qs.config")
Global $settings = _StringExplode(FileRead($handle), "|")
Global $hotkeylast = $settings[1]
HotKeySet("!+qs", "gui")

verify(); We still have to veryify the hotkey set
While 1
   WEnd; keep alive

Func gui()
#Region ### START Koda GUI section ### Form=C:\projects\QuickSilver\mainbox.kxf
$mainbox = GUICreate("QuickSilver", 218, 106, 421, 181)
$dirloc = GUICtrlCreateInput("", 8, 8, 169, 21)
$qdirs = GUICtrlCreateButton("?", 184, 8, 27, 25)
$hotkey = GUICtrlCreateInput("", 8, 40, 169, 21)
$qhotkey = GUICtrlCreateButton("?", 184, 40, 27, 25)
$update = GUICtrlCreateButton("Update Settings", 8, 72, 99, 25)
$min = GUICtrlCreateButton("Minimize Window", 112, 72, 99, 25)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###
verify()
GUICtrlSetData($dirloc, $settings[0])
GUICtrlSetData($hotkey, $settings[1])
While 1
   $nMsg = GUIGetMsg()
   Switch $nMsg
	  Case $GUI_EVENT_CLOSE
		 GUIDelete($mainbox)
		 Return
	  Case $min
		 GUIDelete($mainbox)
		 Return
	  Case $qdirs
		 MsgBox(0, "QuickSilver | Help", "Adding a directory for quicksilver to shred is simple.  Just type in the directory as with any other aplication.  To add multiple directories, simply add a semicolon.  Remember, the shredding is not recursive." & @CRLF & "Example:" & @CRLF & "C:\test\;C:\test2\;C:\hax\")
	  Case $qhotkey
		 MsgBox(0, "QuickSilver | Help", "Setting the hotkey is very easy to do in QuickSilver.  Simply set it to the letter with external keys such as alt or ctrl represented by a different character.  ""!"" is alt, ""+"" is shift, ""^"" is ctrl, and ""#"" is the windows key." & @CRLF & "Example:" & @CRLF & "^!qs")
	  Case $update
		 FileClose($handle)
		 $handle = FileOpen(@scriptdir & "\qs.config", 2)
		 FileWrite($handle, GUICtrlRead($dirloc) & "|" & GUICtrlRead($hotkey) & "|" & md5(GUICtrlRead($dirloc) & "|" & GUICtrlRead($hotkey)))
		 $settings = _StringExplode(FileRead(@scriptdir & "\qs.config"), "|")
		 verify()
		 MsgBox(64, "QuickSilver", "Settings have been updated!")
   EndSwitch
WEnd
EndFunc

Func verify()
   If md5($settings[0] & "|" & $settings[1]) <> $settings[2] Then
	  ConsoleWrite("config file hash should be " & md5($settings[0] & "|" & $settings[1]) & @CRLF)
	  MsgBox(16, "QuickSilver | Error", "Quicksilver has reached an unresolvable error in the config file.  Please restore the qs.config file to its original state!")
	  Exit(2)
   Else
	  HotKeySet($hotkeylast)
	  HotKeySet($settings[1], "shred")
	  $hotkeylast = $settings[1]
	  Return
   EndIf
EndFunc

Func shred()
   TrayTip("QuickSilver", "SHRED JOB QUEUED AND IN PROGRESS!", 100)
   If StringInStr($settings[0], ";") Then
	  $dirlist = _StringExplode($settings[0], ";")
	  For $v in $dirlist
		 $files = _FileListToArray($dirlist[$v])
		 TrayTip("QuickSilver", "PROCESSING FOLDER " & $dirlist[$v], 100)
		 _ArrayDelete($files, 0)
		 For $i in $files
			$size = FileGetSize($dirlist[$v] & $i)
			$rem = FileOpen($dirlist[$v] & $i, 2)
			FileWrite($rem, junkfill($size)); we will override the file with the same number of bytes to prevent from DEM PESKY FORENSIC ANALYSTS
			FileClose($rem); Closing file before we delete, as the delete can't use a handle
;   	    V----// keep commented while testing//----
			FileDelete($settings[0] & $i)
			TrayTip("QuickSilver", "FILE " & $i & " HAS BEEN SCRAMBLED AND CLENSED", 100)
		 Next
	  Next
   Else
	  $files = _FileListToArray($settings[0])
	  _ArrayDelete($files, 0)
	  TrayTip("QuickSilver", "PROCESSING FOLDER " & $settings[0], 100)
	  For $i in $files
		 $size = FileGetSize($settings[0] & $i)
		 $rem = FileOpen($settings[0] & $i, 2)
		 FileWrite($rem, junkfill($size)); we will override the file with the same number of bytes to prevent from DEM PESKY FORENSIC ANALYSTS
		 FileClose($rem); Closing file before we delete, as the delete can't use a handle
;        V----// keep commented while testing//----
		 FileDelete($settings[0] & $i)
		 TrayTip("QuickSilver", "FILE " & $i & " HAS BEEN SCRAMBLED AND CLENSED", 100)
	  Next
   EndIf
EndFunc

Func junkfill($size); one byte THEORETICALY translates into one character that au3 writes.
   Local $data[72] = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "!", "@", "#", "$", "%", "^", "*", "(", ")"]
   $arc = 0
   $returnstr = ""; Only if I could initialize this to a string in au3 without setting it to a null val
   While $arc < $size
	  $returnstr = $returnstr & $data[Random(0, 71, 1)];remember that the 0th slot is the first place in the array (yes au3 does this now).
	  $arc += 1;there isn't a ++ function ;_;
   WEnd
   Return $returnstr
EndFunc